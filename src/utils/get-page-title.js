import defaultSettings from '@/settings';
import i18n from '@/plugins/i18n';

const title = defaultSettings.title || 'vue-example';

export default function getPageTitle(pageName) {
  if (pageName) {
    const pageTitle = i18n.tc(`pages.${pageName.toLowerCase()}`);
    return `${pageTitle} - ${title}`;
  }
  return `${title}`;
}
