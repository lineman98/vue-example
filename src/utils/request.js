import axios from 'axios';
import { Message } from 'element-ui';
import store from '@/store';

// create an axios instance
const service = axios.create({
//  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  baseURL: process.env.API_ENDPOINT,
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 20000, // request timeout
});

// request interceptor
service.interceptors.request.use(
  (config) => {
    if (store.getters.token) { config.headers.Authorization = `Bearer ${store.getters.token}`; }

    // добавляем параметр языка ко всем запросам
    if (!config.params) { config.params = {}; }
    config.params.lang = store.getters.locale;

    return config;
  },
  (error) => {
    // do something with request error
    console.log(error); // for debug
    return Promise.reject(error);
  },
);

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => response,
  (error) => {
    let errorMessage = error.message;
    if (error.response && error.response.data && error.response.data.errors) {
      const errors = error.response.data.errors;
      errorMessage = errors[Object.keys(errors)[0]][0];
    }
    if (error.response && error.response.status === 403 && error.response.data) {
      errorMessage = error.response.data.error || 'Ошибка доступа';
    }
    Message({
      message: errorMessage,
      type: 'error',
      duration: 2000,
    });
    return Promise.reject(error);
  },
);

export default service;
