import request from '@/utils/request';

export async function getQuestions() {
  return new Promise((resolve, reject) => {
    request.get('/faq')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}
