import request from '@/utils/request';

export async function sendClaim(data) {
  return new Promise((resolve, reject) => {
    request.post('/claims', data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}
