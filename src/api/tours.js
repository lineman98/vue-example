import request from '@/utils/request';

export async function fetchMyTours(params) {
  return new Promise((resolve, reject) => {
    request.get('/tours/my', { params })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchAllTours(params) {
  return new Promise((resolve, reject) => {
    request.get('/tours/all', { params })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchTours(params) {
  return new Promise((resolve, reject) => {
    request.get('/tours', { params })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchTour(id) {
  return new Promise((resolve, reject) => {
    request.get(`/tours/${id}`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchTourForModel(id) {
  return new Promise((resolve, reject) => {
    request.get(`/tours/${id}/forModel`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function createTour(data) {
  return new Promise((resolve, reject) => {
    request.post('/tours', data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchTourBill(id) {
  return new Promise((resolve, reject) => {
    request.get(`/tours/${id}/getBill`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function editTour(data) {
  return new Promise((resolve, reject) => {
    request.put(`/tours/${data.id}`, data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function activateTour(id) {
  return new Promise((resolve, reject) => {
    request.post(`/tours/${id}/activate`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function closeTour(id) {
  return new Promise((resolve, reject) => {
    request.post(`/tours/${id}/close`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function blockTour(id) {
  return new Promise((resolve, reject) => {
    request.post(`/tours/${id}/close`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function deleteTour(id) {
  return new Promise((resolve, reject) => {
    request.delete(`/tours/${id}`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}
