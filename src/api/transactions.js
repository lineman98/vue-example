import request from '@/utils/request';

export async function createTransaction(data) {
  return new Promise((resolve, reject) => {
    request.post('/transactions/check', data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function payBillFromBalance(id) {
  return new Promise((resolve, reject) => {
    request.get(`/transactions/payBillFromBalance/${id}`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}
