import {
  fetchMyOffers,
  fetchOffers,
  createOffer,
  fetchOffer,
  fetchOfferForModel,
  deleteOffer,
  agreeOfferUser,
  refuseOfferUser,
  agreeOfferFromManager,
  refuseOfferFromManager, agreeOfferFromModel, refuseOfferFromModel,
  closeOffer,
  endOffer,
  updateOffer,
  sendOfferApplication, ignoreOfferApplication,
  setViewedOffer
} from '@/api/offers';

import EventBus from '@/bus';

const state = {
  offers: [],
  currentOffer: {},
};

const mutations = {
  SET_OFFERS: (state, offers) => {
    state.offers = offers;
  },
  PREPEND_OFFER: (state, offer) => {
    state.offers.splice(0, 0, offer);
  },
  SET_CURRENT_OFFER: (state, offer) => {
    if(offer !== undefined) {
      state.currentOffer = offer;
    }
  },

  AGREE_OFFER_FROM_MODEL: (state, offer_link_id) => {
    const offerIndex = state.offers.findIndex(el => el.offer_link_id === offer_link_id);
    state.offers[offerIndex].offer_link_status = 'model_agreed';
    state.currentOffer.offer_link_status = 'model_agreed';
  },

  AGREE_OFFER_USER: (state, {offerId, userId}) => {
    const offerIndex = state.offers.findIndex(el => el.id === offerId);
    const offerUser = state.offers[offerIndex].users_requesting.find(user => user.id == userId)
    state.offers[offerIndex].users_requesting = state.offers[offerIndex].users_requesting.filter((user) => user.id !== userId)
    state.offers[offerIndex].users_going.push(offerUser)
    console.log(state.offers[offerIndex])
  },

  SET_VIEWED(state, offer_id) {
    const offerIndex = state.offers.findIndex(offer => offer.id == offer_id)
    if(offerIndex !== -1) {
      state.offers[offerIndex].viewed = true
    }
  },

  REFUSE_OFFER_USER: (state, {offerId, userId}) => {
    const offerIndex = state.offers.findIndex(el => el.id === offerId);
    const offerUser = state.offers[offerIndex].users_requesting.find(user => user.id == userId)
    state.offers[offerIndex].users_requesting = state.offers[offerIndex].users_requesting.filter((user) => user.id !== userId)
  },

  REFUSE_OFFER_FROM_MODEL: (state, offer_link_id) => {
    const offerIndex = state.offers.findIndex(el => el.offer_link_id === offer_link_id);
    state.offers[offerIndex].offer_link_status = 'model_refused';
    state.currentOffer.offer_link_status = 'model_refused';
  },

  UPDATE_OFFER: (state, offer) => {
    if(state.currentOffer.id == offer.id) {
      for(let i of Object.keys(offer)) {
        state.currentOffer[i] = offer[i]
      }
    }

    const offerIndex = state.offers.findIndex(el => el.id === offer.id)
    if(offerIndex > -1) {
      for(let i in Object.keys(offer)) {
        state.offers[offerIndex][i] = offer[i]
      }
    }
  },

  REFUSE_OFFER_FROM_MANAGER: (state, offer_link_id) => {
    // ищем модель в текущем оффере, удаляем эту модель из списка
    const modelIndex = state.currentOffer.agreed_models.findIndex(el => el.offer_link_id === offer_link_id);
    if (modelIndex !== -1) {
      state.currentOffer.agreed_models.splice(modelIndex, 1);
    }
    // уменьшаем количество ожидающих моделей в общем списке предложений
    const offerIndex = state.offers.findIndex(el => el.id === state.currentOffer.id);
    if (offerIndex !== -1) {
      state.offers[offerIndex].agreed_models_count--;
    }
  },

  AGREE_OFFER_FROM_MANAGER: (state, offer_link_id) => {
    const modelIndex = state.currentOffer.agreed_models.findIndex(el => el.offer_link_id === offer_link_id);
    if (modelIndex !== -1) {
      const model = { ...state.currentOffer.agreed_models[modelIndex], offer_link_status: 'manager_agreed' };

      // удаляем модель из списка ожидающих
      state.currentOffer.agreed_models.splice(modelIndex, 1);
      // добавляем модель в список принятых моделей
      state.currentOffer.agreed_by_manager_models.push(model);
    }

    // уменьшаем количество ожидающих моделей в общем списке предложений
    const offerIndex = state.offers.findIndex(el => el.id === state.currentOffer.id);
    if (offerIndex !== -1) {
      state.offers[offerIndex].agreed_models_count--;
    }
  },

  DELETE_OFFER: (state, offer_id) => {
    const offerIndex = state.offers.findIndex(el => el.id === offer_id);
    if (offerIndex !== -1) {
      state.offers.splice(offerIndex, 1);
    }
  },

  CLOSE_OFFER: (state, offer_id) => {
    const offerIndex = state.offers.findIndex(el => el.id === offer_id);
    if (offerIndex !== -1) {
      state.offers.splice(offerIndex, 1);
    }
    if(state.currentOffer.id == offer_id) {
      state.currentOffer.offer_model_status = 'closed'
    }
  },

  END_OFFER: (state, offer_id) => {
    const offerIndex = state.offers.findIndex(el => el.id === offer_id);
    if (offerIndex !== -1) {
      state.offers[offerIndex].offer_model_status = 'ended';
    }
    if(state.currentOffer.id == offer_id) {
      state.currentOffer.offer_model_status = 'ended'
    }
  },

  SET_APPLICATION_UPDATE_STATE: (state, offer_user) => {
    state.currentOffer.application = offer_user;

    const offerIndex = state.offers.findIndex(offer => offer.id == offer_user.offer_id)
    console.log(offerIndex, state.offers[offerIndex])
    if (offerIndex !== -1) {
      state.offers[offerIndex].application = offer_user
    }
  },

  SET_OFFER_STATUS: (state, { offer_id, status }) => {
    if (state.currentOffer && state.currentOffer.id === offer_id) {
      state.currentOffer.status = status;
    }
    const offerIndex = state.offers.findIndex(el => el.id === offer_id);
    if (offerIndex !== -1) {
      state.offers[offerIndex].status = status;
    }
  },

  ADD_AGREED_MODEL_TO_OFFER: (state, { offer, model }) => {
    // увеличиваем количество ожидающих моделей в общем списке предложений
    const offerIndex = state.offers.findIndex(el => el.id === offer.id);
    if (offerIndex !== -1) {
      state.offers[offerIndex].agreed_models_count++;
    }
    // обновляем открытый оффер
    if (state.currentOffer && state.currentOffer.id === offer.id) {
      EventBus.$emit('reloadCurrentOffer');
    }
  },

  SET_MANAGER_AGREED_TO_OFFER: (state, offer_id) => {
    const offerIndex = state.offers.findIndex(el => el.id === offer_id);
    if (offerIndex !== -1) {
      state.offers[offerIndex].offer_link_status = 'manager_agreed';
    }
  },

  SET_MANAGER_REFUSE_TO_OFFER: (state, offer_id) => {
    const offerIndex = state.offers.findIndex(el => el.id === offer_id);
    if (offerIndex !== -1) {
      state.offers[offerIndex].offer_link_status = 'manager_refused';
    }
  },

  PREPEND_OFFER_APPLICATION: (state, application) => {
    if(state.currentOffer.id == application.offer_id) {
      state.currentOffer.users_requesting.unshift(application.user)
    }

    const offerIndex = state.offers.findIndex(el => el.id === application.offer_id);
    if(offerIndex !== -1) {
      state.offers[offerIndex].users_requesting.unshift(application.user)
    }
  },

  SET_OFFER_APPLICATION: (state, {offer_id, application}) => {
    if(state.currentOffer.id == offer_id) {
      state.currentOffer.application = application
    }

    const offerIndex = state.offers.findIndex(el => el.id === application.offer_id);
    if(offerIndex !== -1) {
      state.offers[offerIndex].application = application
    }
  }
};

const actions = {
  async loadMyOffers({ commit }, params) {
    return new Promise((resolve, reject) => {
      fetchMyOffers(params)
        .then((response) => {
          commit('SET_OFFERS', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async loadOffers({ commit }, params) {
    return new Promise((resolve, reject) => {
      fetchOffers(params)
        .then((response) => {
          commit('SET_OFFERS', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async createOffer({ commit }, data) {
    return new Promise((resolve, reject) => {
      createOffer(data)
        .then((response) => {
          //commit('PREPEND_OFFER', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async loadOffer({ commit }, id) {
    return new Promise((resolve, reject) => {
      fetchOffer(id)
        .then((response) => {
          commit('SET_CURRENT_OFFER', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async loadOfferModel({ commit }, id) {
    return new Promise((resolve, reject) => {
      fetchOfferForModel(id)
        .then((response) => {
          commit('SET_CURRENT_OFFER', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async setViewed({ commit }, id) {
    return new Promise((resolve, reject) => {
      setViewedOffer(id)
        .then((response) => {
          resolve(response.data);
        }).catch((error) => {
        reject(error);
      });
    });
  },

  async deleteOffer({ commit }, id) {
    return new Promise((resolve, reject) => {
      deleteOffer(id)
        .then((response) => {
          commit('DELETE_OFFER', id);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  prependNewOffer({ commit }, offer) {
    commit('PREPEND_OFFER', offer);
  },

  changeOfferStatus({ commit }, { offer_id, status }) {
    commit('SET_OFFER_STATUS', { offer_id, status });
  },

  async agreeFromManager({ commit }, offer_link_id) {
    return new Promise((resolve, reject) => {
      agreeOfferFromManager(offer_link_id)
        .then((response) => {
          commit('AGREE_OFFER_FROM_MANAGER', offer_link_id);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async agreeOfferUser({ commit }, params) {
    return new Promise((resolve, reject) => {
      agreeOfferUser(params.offerId, params.userId)
        .then((response) => {
          commit('AGREE_OFFER_USER', params);
          resolve(response.data);
        }).catch((error) => {
        reject(error);
      });
    });
  },

  async refuseOfferUser({ commit }, params) {
    return new Promise((resolve, reject) => {
      refuseOfferUser(params.offerId, params.userId)
        .then((response) => {
          commit('REFUSE_OFFER_USER', params);
          resolve(response.data);
        }).catch((error) => {
        reject(error);
      });
    });
  },

  async refuseFromManager({ commit }, offer_link_id) {
    return new Promise((resolve, reject) => {
      refuseOfferFromManager(offer_link_id)
        .then((response) => {
          commit('REFUSE_OFFER_FROM_MANAGER', offer_link_id);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async agreeFromModel({ commit }, offer_link_id) {
    return new Promise((resolve, reject) => {
      agreeOfferFromModel(offer_link_id)
        .then((response) => {
          commit('AGREE_OFFER_FROM_MODEL', offer_link_id);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async refuseFromModel({ commit }, offer_link_id) {
    return new Promise((resolve, reject) => {
      refuseOfferFromModel(offer_link_id)
        .then((response) => {
          commit('REFUSE_OFFER_FROM_MODEL', offer_link_id);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async close({ commit }, id) {
    return new Promise((resolve, reject) => {
      closeOffer(id)
        .then((response) => {
          commit('CLOSE_OFFER', id);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async end({ commit }, id) {
    return new Promise((resolve, reject) => {
      endOffer(id)
        .then((response) => {
          commit('END_OFFER', id);
          resolve(response.data);
        }).catch((error) => {
        reject(error);
      });
    });
  },

  async sendApplication({ commit }, offer_id) {
    return new Promise((resolve, reject) => {
      sendOfferApplication(offer_id)
        .then((response) => {
          commit('SET_APPLICATION_UPDATE_STATE', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async update({ commit }, offer) {
    return new Promise((resolve, reject) => {
      updateOffer(offer)
        .then((response) => {
          commit('UPDATE_OFFER', offer);
          resolve(response.data);
        }).catch((error) => {
        reject(error);
      });
    });
  },

  async ignoreApplication({ commit }, offer_id) {
    return new Promise((resolve, reject) => {
      ignoreOfferApplication(offer_id)
        .then((response) => {
          commit('SET_APPLICATION_UPDATE_STATE', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  // добавление заявки на участие во вписку
  newOfferApplication({ commit }, application) {
    commit('PREPEND_OFFER_APPLICATION', application)
  },

  //обновление статуса нашей заявки на вписку
  updateOfferApplication({ commit }, {offer_id, application }) {
    commit('SET_OFFER_APPLICATION', { offer_id, application })
  },

  // добавление согласившейся модели в предложение
  addAgreedModelToOffer({ commit }, { offer, model }) {
    commit('ADD_AGREED_MODEL_TO_OFFER', { offer, model });
  },

  // модель утвердили на предложение
  setManagerAgreedOfferModel({ commit }, offer_id) {
    commit('SET_MANAGER_AGREED_TO_OFFER', offer_id);
  },

  // модель отказали на предложение
  setManagerRefuseOfferModel({ commit }, offer_id) {
    commit('SET_MANAGER_REFUSE_TO_OFFER', offer_id);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};

